﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planta_Isabelle_1705491
{
	interface IPlanta
	{
		string GerarHash();
		List<object> GerarLista();
	}
}
